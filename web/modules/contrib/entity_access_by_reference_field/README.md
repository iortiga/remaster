# Entity Access by Reference Field

**Allows to control access to entities based on entity reference fields.** With this module, you can define access conditions on entity reference fields for the host entity.

## Requirements

This module does not require any other modules or libraries to work.

## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).

## Configuration

When creating an entity reference field, go to field settings and enable the
"Enable referenced entity permission" checkbox. After that, the configuration
for this field pops up.

### Configurable examples:

* If a user is allowed to **_view_ the referenced entity**, he is also allowed to **_view_ the host entity**.
* If a user is allowed to **_edit_ the referenced entity**, he is also allowed to **_edit_ the host entity**.
* If a user is allowed to **_delete_ the referenced entity**, he is also allowed to **_delete_ the host entity**.
* If a user is allowed to **_edit_ the referenced entity**, he is also allowed to **_edit_ or _delete_ the host entity**.
* If a user **is the referenced entity**, he is allowed to **_edit_ the host entity**.

The fallback behavior (allow, neutral, deny) and the empty behaviour (if a host entity does not have a referenced entity set) is also defined per field. **Note, that this module uses third party settings on the field storage, meaning multiple field instances of the same field will share the settings!**

### Currently supported access checks:

*   View
*   View Unpublished
*   Edit
*   Delete
*   Is User (referenced user entity only)

### Future plans (if community helps):

If the community helps to develop the functionality or development is sponsored, there are further ideas that might be added here, like

*   Condition: Current user created the referenced entity

... any other ideas? Please create an issue and help to develop the functionality.

## History & reasons for this module

This module was born, as we already had experience and a good starting point with our [Entity Access by Role Field](https://www.drupal.org/project/entity_access_by_role_field) module. In Drupal 7 we loved to use [Node access node reference](/project/nodeaccess_nodereference) and [Node access user reference](https://www.drupal.org/project/nodeaccess_userreference) which have no Drupal 8+ release. We tried [Access by Reference](https://www.drupal.org/project/access_by_ref) module, but it didn't work really well. https://www.drupal.org/project/reference\_access does similar things, just the other way around (referencing from the user page). As we didn't find a good (enough) alternative, we decided to create this module based on our existing [Entity Access by Role Field](https://www.drupal.org/project/entity_access_by_role_field) code and knowledge.

## Debugging permissions / entity access

For debugging permissions on entities, the following modules can be helpful:

1.  [Devel](https://www.drupal.org/project/devel)
2.  [Web Profiler](https://www.drupal.org/project/webprofiler)
3.  [Drush Tools](https://www.drupal.org/project/drush_tools)
4.  [Masquerade](https://www.drupal.org/project/masquerade)

## Similar modules & alternatives

You may want to have a look at these alternatives, before making the choice:

*   [Access by Reference](https://www.drupal.org/project/access_by_ref) (similar but different UX)
*   [Reference Access](https://www.drupal.org/project/reference_access) (vice versa)
*   [Group](https://www.drupal.org/project/group) (Very complex and flexible)

**For Drupal 7 there were some more helpful modules like:**

*   [Nodeaccess](https://www.drupal.org/project/nodeaccess)
*   [Node access node reference](/project/nodeaccess_nodereference)
*   [Node access user reference](https://www.drupal.org/project/nodeaccess_userreference)
*   [Node Access Relation](https://www.drupal.org/project/node_access_relation)
*   [Nodeaccess Autoreference](https://www.drupal.org/project/nodeaccess_autoreference)

**For other use-cases:**

*   [Entity Access by Role Field](https://www.drupal.org/project/entity_access_by_role_field)
*   [Content Access](https://www.drupal.org/project/content_access)
*   [Node View Permissions](https://www.drupal.org/project/node_view_permissions)
*   [Field Permissions](https://www.drupal.org/project/field_permissions)
*   [View unpublished](https://www.drupal.org/project/view_unpublished)
*   [Flexible Permissions](https://www.drupal.org/project/flexible_permissions)
*   [Access Policy](https://www.drupal.org/project/access_policy)

## Need to select roles instead (per entitiy)

If you need to grant access (CRUD) to single entities by selecting roles instead in a flexible way, instead have a look at [Entity Access by Role Field Module](https://www.drupal.org/project/entity_access_by_role_field) instead, which provides such functionality on role reference fields.

## Maintainers

- Julian Pustkuchen - [Anybody](https://www.drupal.org/u/anybody)
- Joshua Sedler - [Grevil](https://www.drupal.org/u/grevil)
- Thomas Frobieter - [thomas.frobieter](https://www.drupal.org/u/thomasfrobieter)

## Supporting organizations:

[webks: websolutions kept simple](http://www.webks.de "Webdesigner & Webentwickler aus Porta Westfalica bei Minden, Herford, Bad Oeynhausen, Schaumburg, Bielefeld in Ostwestfalen") (https://www.webks.de) and [DROWL: Drupalbasierte Lösungen aus Ostwestfalen-Lippe (OWL), Germany](http://www.drowl.de "Drupal CMS Entwickler & Designer aus Ostwestfalen-Lippe (OWL): Bielefeld, Paderborn, Gütersloh, Minden, Detmold, Herford. Ihre Drupal CMS Experten seit 2007.") (https://www.drowl.de)
