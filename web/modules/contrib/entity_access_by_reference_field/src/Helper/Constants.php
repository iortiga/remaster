<?php

namespace Drupal\entity_access_by_reference_field\Helper;

/**
 * Common used constants for the 'entity_access_by_reference_field' module.
 */
class Constants {
  public const FALLBACK_NEUTRAL = 'neutral';
  public const FALLBACK_ALLOWED = 'allowed';
  public const FALLBACK_FORBIDDEN = 'forbidden';

  public const OPERATION_VIEW = 'view';
  // Helper operation:
  public const OPERATION_VIEW_UNPUBLISHED = 'view_unpublished';
  public const OPERATION_UPDATE = 'update';
  public const OPERATION_DELETE = 'delete';

  public const IS_REFERENCED_USER = 'is_referenced_user';

  public const GLOBAL_PERMISSION = 'bypass entity_access_by_reference_field permissions';

}
