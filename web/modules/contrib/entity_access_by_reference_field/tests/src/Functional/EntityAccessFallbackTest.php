<?php

namespace Drupal\Tests\entity_access_by_reference_field\Functional;

use Drupal\entity_access_by_reference_field\Helper\Constants;
use Drupal\user\Entity\Role;

/**
 * This class provides access tests for entity_access_by_reference_field.
 *
 * @group entity_access_by_reference_field
 */
class EntityAccessFallbackTest extends EntityAccessTestBase {

  /**
   * {@inheritDoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->createContentType(['type' => 'notice']);
  }

  /**
   * Setup fallback tests.
   */
  public function setupFallbackTests() {
    $this->createEntityReferenceField('node', 'article', 'field_notice_test', 'field_notice_test', 'node', 'default', ['target_bundles' => ['notice']]);
    $this->fieldStorage = \Drupal::entityTypeManager()->getStorage('field_storage_config')->load('node.field_notice_test');

    // Create a notice:
    $referencedNode = $this->drupalCreateNode([
      'id' => 1,
      'title' => 'Notice',
      'type' => 'notice',
      'body' => 'Body field value.',
    ]);
    $referencedNode->save();

    // Create an article referencing the notice:
    $node = $this->drupalCreateNode([
      'id' => 2,
      'title' => 'My Test Node',
      'type' => 'article',
      'body' => 'Body field value.',
      'field_notice_test' => [
        0 => [
          'target_id' => $referencedNode->id(),
        ],
      ],
    ]);
    $node->save();
  }

  /**
   * Setup fallback empty tests.
   */
  public function setupFallbackEmptyTests() {
    $this->createEntityReferenceField('node', 'article', 'field_notice_test', 'field_notice_test', 'node', 'default', ['target_bundles' => ['notice']]);
    $this->fieldStorage = \Drupal::entityTypeManager()->getStorage('field_storage_config')->load('node.field_notice_test');

    // Create a notice:
    $referencedNode = $this->drupalCreateNode([
      'id' => 1,
      'title' => 'Notice',
      'type' => 'notice',
      'body' => 'Body field value.',
    ]);
    $referencedNode->save();

    // Create an article referencing the notice:
    $node = $this->drupalCreateNode([
      'id' => 2,
      'title' => 'My Test Node',
      'type' => 'article',
      'body' => 'Body field value.',
      'field_notice_test' => [
        0 => [
          'target_id' => NULL,
        ],
      ],
    ]);
    $node->save();
  }

  /**
   * Test fallback neutral.
   */
  public function testFallbackNeutral() {
    $this->drupalLogin($this->authenticatedUser);
    $this->setupFallbackTests();

    // Allow none $permissionMatrixValues:
    $permissionMatrixValues = $this->initiatePermissionMatrix();

    $this->setupThirdPartySettings($this->fieldStorage, 'or', TRUE, $permissionMatrixValues, Constants::FALLBACK_NEUTRAL);

    // Check if the edit page is not accessible:
    $this->drupalGet('/node/2/edit');
    $this->assertSession()->statusCodeEquals(403);

    // Grant permission to edit the referenced article:
    $authenticatedRole = Role::load('authenticated');
    $authenticatedRole->grantPermission('access content')->save();
    $authenticatedRole->grantPermission('edit any notice content')->save();

    // The edit page should not be accessible, as fallback neutral lets the
    // "edit any article content" permission decide and that permission is not
    // set on the current user:
    $this->drupalGet('/node/2/edit');
    $this->assertSession()->statusCodeEquals(403);

    // Node 1 should be accessible, as it is a notice, and the user has this
    // permission:
    $this->drupalGet('/node/1/edit');
    $this->assertSession()->statusCodeEquals(200);
  }

  /**
   * Test fallback allowed.
   */
  public function testFallbackAllowed() {
    $this->drupalLogin($this->authenticatedUser);
    $this->setupFallbackTests();

    // Allow none $permissionMatrixValues:
    $permissionMatrixValues = $this->initiatePermissionMatrix();

    // Check if the edit page is not accessible:
    $this->drupalGet('/node/2/edit');
    $this->assertSession()->statusCodeEquals(403);
    // The delete page aswell:
    $this->drupalGet('/node/2/delete');
    $this->assertSession()->statusCodeEquals(403);

    $this->setupThirdPartySettings($this->fieldStorage, 'or', TRUE, $permissionMatrixValues, Constants::FALLBACK_ALLOWED);
    $authenticatedRole = Role::load('authenticated');
    // Grant access content, as this permission overrides any other permission:
    $authenticatedRole->grantPermission('access content')->save();

    // The article edit page should be accessible, as the fallback is allowed:
    $this->drupalGet('/node/2/edit');
    $this->assertSession()->statusCodeEquals(200);

    // The delete page aswell:
    $this->drupalGet('/node/2/delete');
    $this->assertSession()->statusCodeEquals(200);
  }

  /**
   * Test fallback forbidden.
   */
  public function testFallbackForbidden() {
    $this->drupalLogin($this->authenticatedUser);
    $this->setupFallbackTests();

    // Allow none $permissionMatrixValues:
    $permissionMatrixValues = $this->initiatePermissionMatrix();

    // Check if the edit page is not accessible:
    $this->drupalGet('/node/2/edit');
    $this->assertSession()->statusCodeEquals(403);
    // The delete page aswell:
    $this->drupalGet('/node/2/delete');
    $this->assertSession()->statusCodeEquals(403);

    $this->setupThirdPartySettings($this->fieldStorage, 'or', TRUE, $permissionMatrixValues, Constants::FALLBACK_FORBIDDEN);
    $authenticatedRole = Role::load('authenticated');
    // Grant all article and notice permissions, the access should still be
    // forbidden:
    $authenticatedRole
      ->grantPermission('access content')
      ->grantPermission('delete any article content')
      ->grantPermission('delete any notice content')
      ->grantPermission('edit any article content')
      ->grantPermission('edit any notice content')
      ->save();

    // The article edit page should not be accessible:
    $this->drupalGet('/node/2/edit');
    $this->assertSession()->statusCodeEquals(403);
    // The delete page aswell:
    $this->drupalGet('/node/2/delete');
    $this->assertSession()->statusCodeEquals(403);
    // And the view page:
    $this->drupalGet('/node/2');
    $this->assertSession()->statusCodeEquals(403);
  }

  /**
   * Tests if the fallback serves as a fallback and does not ALWAYS be used.
   */
  public function testFallbackIgnored() {
    $this->drupalLogin($this->authenticatedUser);
    $this->setupFallbackTests();

    // Initiate $permissionMatrixValues:
    $permissionMatrixValues = $this->initiatePermissionMatrix();
    // Allow update permission only:
    $permissionMatrixValues['columns']['update']['update'] = TRUE;

    // Check if the edit page is not accessible:
    $this->drupalGet('/node/2/edit');
    $this->assertSession()->statusCodeEquals(403);
    // The delete page aswell:
    $this->drupalGet('/node/2/delete');
    $this->assertSession()->statusCodeEquals(403);

    $this->setupThirdPartySettings($this->fieldStorage, 'or', TRUE, $permissionMatrixValues, Constants::FALLBACK_FORBIDDEN);
    $authenticatedRole = Role::load('authenticated');
    // Grant all article and notice permissions, the access should still be
    // forbidden for all except edit:
    $authenticatedRole
      ->grantPermission('access content')
      ->grantPermission('delete any article content')
      ->grantPermission('delete any notice content')
      ->grantPermission('edit any article content')
      ->grantPermission('edit any notice content')
      ->save();

    // Only edit should be allowed, for the rest, the fallback applies, even
    // if the user has access to any page due the "administer nodes" permission.
    $this->drupalGet('/node/2/edit');
    $this->assertSession()->statusCodeEquals(200);
    // The delete page aswell:
    $this->drupalGet('/node/2/delete');
    $this->assertSession()->statusCodeEquals(403);
    // And the view page:
    $this->drupalGet('/node/2');
    $this->assertSession()->statusCodeEquals(403);
  }

  /**
   * Test fallback empty neutral.
   */
  public function testFallbackEmptyNeutral() {
    $this->drupalLogin($this->authenticatedUser);
    $this->setupFallbackEmptyTests();

    // Allow none $permissionMatrixValues:
    $permissionMatrixValues = $this->initiatePermissionMatrix();

    $this->setupThirdPartySettings($this->fieldStorage, 'or', TRUE, $permissionMatrixValues, Constants::FALLBACK_NEUTRAL, Constants::FALLBACK_NEUTRAL);

    // Check if the edit page is not accessible:
    $this->drupalGet('/node/2/edit');
    $this->assertSession()->statusCodeEquals(403);

    // Grant permission to edit the referenced article:
    $authenticatedRole = Role::load('authenticated');
    $authenticatedRole->grantPermission('access content')->save();
    $authenticatedRole->grantPermission('edit any notice content')->save();
    $authenticatedRole->grantPermission('delete any article content')->save();

    // The edit page should not be accessible, as fallback neutral lets the
    // "edit any article content" permission decide and that permission is not
    // set on the current user:
    $this->drupalGet('/node/2/edit');
    $this->assertSession()->statusCodeEquals(403);
    // Delete should be accessible, as the user has the correct article
    // permission:
    $this->drupalGet('/node/2/delete');
    $this->assertSession()->statusCodeEquals(200);
  }

  /**
   * Test fallback allowed.
   */
  public function testFallbackEmptyAllowed() {
    $this->drupalLogin($this->authenticatedUser);
    $this->setupFallbackEmptyTests();

    // Allow none $permissionMatrixValues:
    $permissionMatrixValues = $this->initiatePermissionMatrix();

    // Check if the edit page is not accessible:
    $this->drupalGet('/node/2/edit');
    $this->assertSession()->statusCodeEquals(403);
    // The delete page aswell:
    $this->drupalGet('/node/2/delete');
    $this->assertSession()->statusCodeEquals(403);

    $this->setupThirdPartySettings($this->fieldStorage, 'or', TRUE, $permissionMatrixValues, Constants::FALLBACK_NEUTRAL, Constants::FALLBACK_ALLOWED);
    $authenticatedRole = Role::load('authenticated');
    // Grant access content, as this permission overrides any other permission:
    $authenticatedRole->grantPermission('access content')->save();

    // The article edit page should be accessible, as the fallback empty is
    // allowed:
    $this->drupalGet('/node/2/edit');
    $this->assertSession()->statusCodeEquals(200);

    // The delete page aswell:
    $this->drupalGet('/node/2/delete');
    $this->assertSession()->statusCodeEquals(200);
  }

  /**
   * Test fallback empty forbidden.
   */
  public function testFallbackEmptyForbidden() {
    $this->drupalLogin($this->authenticatedUser);
    $this->setupFallbackEmptyTests();

    // Allow none $permissionMatrixValues:
    $permissionMatrixValues = $this->initiatePermissionMatrix();

    // Check if the edit page is not accessible:
    $this->drupalGet('/node/2/edit');
    $this->assertSession()->statusCodeEquals(403);
    // The delete page aswell:
    $this->drupalGet('/node/2/delete');
    $this->assertSession()->statusCodeEquals(403);

    $this->setupThirdPartySettings($this->fieldStorage, 'or', TRUE, $permissionMatrixValues, Constants::FALLBACK_NEUTRAL, Constants::FALLBACK_FORBIDDEN);
    $authenticatedRole = Role::load('authenticated');
    // Grant master permissions, the access should still be forbidden:
    $authenticatedRole->grantPermission('access content')->save();
    $authenticatedRole->grantPermission('administer nodes')->save();

    // We should not be able to access these pages, because of the empty
    // fallback:
    $this->drupalGet('/node/2/edit');
    $this->assertSession()->statusCodeEquals(403);
    // The delete page aswell:
    $this->drupalGet('/node/2/delete');
    $this->assertSession()->statusCodeEquals(403);
    // And the view page:
    $this->drupalGet('/node/2/delete');
    $this->assertSession()->statusCodeEquals(403);
  }

}
