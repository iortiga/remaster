<?php

namespace Drupal\Tests\entity_access_by_reference_field\Functional\Media;

use Drupal\media\Entity\Media;
use Drupal\user\Entity\Role;

/**
 * This class provides access tests for entity_access_by_reference_field media.
 *
 * @group entity_access_by_reference_field
 */
class MediaReferencingUserAccessTest extends MediaEntityAccessTestBase {

  /**
   * The user who gets referenced.
   *
   * @var \Drupal\user\Entity\User
   */
  protected $referencedUser;

  /**
   * Setup media referencing content tests.
   */
  public function setupMediaReferencingUser() {
    $this->createEntityReferenceField('media', 'test', 'field_user_test', 'field_user_test', 'user', 'default', ['target_bundles' => ['user']]);
    $this->fieldStorage = \Drupal::entityTypeManager()->getStorage('field_storage_config')->load('media.field_user_test');

    // Create user:
    $this->referencedUser = $this->createUser([], 'referencedUser', FALSE, ['uid' => 5]);
    $this->referencedUser->save();

    // Create Media instance:
    $media = Media::create([
      'mid' => 1,
      'bundle' => 'test',
      'uid' => $this->adminUser->id(),
      'name' => 'My Test Media',
      'field_user_test' => [
        0 => [
          'target_id' => $this->referencedUser->id(),
        ],
      ],
    ]);
    $media->save();
  }

  /**
   * {@inheritDoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->setupMediaReferencingUser();
  }

  /**
   * Test, if a media can be accessed.
   *
   * Tests, if a media referencing a user can be viewed, when the "view"
   * permission is given.
   */
  public function testAccessViewOnMediaReferencingUserViewAllowed() {
    $this->drupalLogin($this->authenticatedUser);

    // Allow all $permissionMatrixValues:
    $permissionMatrixValues = $this->initiatePermissionMatrixWithUserValues();

    // Give the permission to update the entity if the user is allowed to
    // update the referenced entity:
    $permissionMatrixValues['columns']['view']['view'] = TRUE;
    $this->setupThirdPartySettings($this->fieldStorage, 'or', TRUE, $permissionMatrixValues);

    // The view page should not be accessible:
    $this->drupalGet('/media/1');
    $this->assertSession()->statusCodeEquals(403);
    // Check if the edit page is not accessible:
    $this->drupalGet('/media/1/edit');
    $this->assertSession()->statusCodeEquals(403);
    // The delete page should not be accessible:
    $this->drupalGet('/media/1/delete');
    $this->assertSession()->statusCodeEquals(403);

    // Grant permission to edit the referenced article:
    $authenticatedRole = Role::load('authenticated');
    $authenticatedRole->grantPermission('access user profiles')->save();

    // The view page should be accessible:
    $this->drupalGet('/media/1');
    $this->assertSession()->statusCodeEquals(200);
    // The edit page should still not be accessible:
    $this->drupalGet('/media/1/edit');
    $this->assertSession()->statusCodeEquals(403);
    // The delete page should still not be accessible:
    $this->drupalGet('/media/1/delete');
    $this->assertSession()->statusCodeEquals(403);
  }

  /**
   * Test, if a media can be accessed.
   *
   * Tests, if a media referencing a user can be edited, when the "edit"
   * permission is given.
   */
  public function testAccessAllOnMediaReferencingUserAllAllowed() {
    $this->drupalLogin($this->authenticatedUser);

    // Allow all $permissionMatrixValues:
    $permissionMatrixValues = $this->initiatePermissionMatrixWithUserValues();

    // Give the permission to update the entity if the user is allowed to
    // update the referenced entity:
    $permissionMatrixValues['columns']['update']['update'] = TRUE;
    $permissionMatrixValues['columns']['delete']['delete'] = TRUE;
    $permissionMatrixValues['columns']['view']['view'] = TRUE;
    $this->setupThirdPartySettings($this->fieldStorage, 'or', TRUE, $permissionMatrixValues);

    // The view page should not be accessible:
    $this->drupalGet('/media/1');
    $this->assertSession()->statusCodeEquals(403);
    // Check if the edit page is not accessible:
    $this->drupalGet('/media/1/edit');
    $this->assertSession()->statusCodeEquals(403);
    // The delete page should not be accessible:
    $this->drupalGet('/media/1/delete');
    $this->assertSession()->statusCodeEquals(403);

    // Grant permission to edit the referenced article:
    $authenticatedRole = Role::load('authenticated');
    $authenticatedRole->grantPermission('administer users')->save();

    // The view page should be accessible:
    $this->drupalGet('/user/1');
    $this->assertSession()->statusCodeEquals(200);
    // The edit page should be accessible:
    $this->drupalGet('/media/1/edit');
    $this->assertSession()->statusCodeEquals(200);
    // The delete page should be accessible:
    $this->drupalGet('/media/1/delete');
    $this->assertSession()->statusCodeEquals(200);
  }

  /**
   * Test, if a media can be accessed.
   *
   * Tests, if a media referencing a user can be accessed, if the current logged
   * in user is that very user.
   */
  public function testAccessAllOnMediaReferencingCurrentUser() {
    $this->drupalLogin($this->authenticatedUser);

    // Allow all $permissionMatrixValues:
    $permissionMatrixValues = $this->initiatePermissionMatrixWithUserValues();

    // Give the permission to update the entity if the user is allowed to
    // update the referenced entity:
    $permissionMatrixValues['columns']['update']['is_referenced_user'] = TRUE;
    $permissionMatrixValues['columns']['delete']['is_referenced_user'] = TRUE;
    $permissionMatrixValues['columns']['view']['is_referenced_user'] = TRUE;
    $this->setupThirdPartySettings($this->fieldStorage, 'or', TRUE, $permissionMatrixValues);

    // The view page should not be accessible:
    $this->drupalGet('/media/1');
    $this->assertSession()->statusCodeEquals(403);
    // Check if the edit page is not accessible:
    $this->drupalGet('/media/1/edit');
    $this->assertSession()->statusCodeEquals(403);
    // The delete page should not be accessible:
    $this->drupalGet('/media/1/delete');
    $this->assertSession()->statusCodeEquals(403);

    // Switch to the referenced user, they should have access:
    $this->drupalLogout();
    $this->drupalLogin($this->referencedUser);

    // The view page should be accessible now:
    $this->drupalGet('/media/1');
    $this->assertSession()->statusCodeEquals(200);
    // The edit page should also be accessible:
    $this->drupalGet('/media/1/edit');
    $this->assertSession()->statusCodeEquals(200);
    // The delete page should also be accessible:
    $this->drupalGet('/media/1/delete');
    $this->assertSession()->statusCodeEquals(200);
  }

}
