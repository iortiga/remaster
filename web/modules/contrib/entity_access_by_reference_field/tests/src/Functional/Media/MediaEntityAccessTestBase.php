<?php

namespace Drupal\Tests\entity_access_by_reference_field\Functional\Media;

use Drupal\Tests\entity_access_by_reference_field\Functional\EntityAccessTestBase;
use Drupal\Tests\media\Traits\MediaTypeCreationTrait;

/**
 * This class provides access tests for entity_access_by_reference_field media.
 *
 * @group entity_access_by_reference_field
 */
abstract class MediaEntityAccessTestBase extends EntityAccessTestBase {
  use MediaTypeCreationTrait;

  /**
   * Further modules to enable.
   *
   * @var array
   */
  protected static $modules = [
    'media',
    // Needed for 'test' media types:
    'media_test_source',
  ];

  /**
   * {@inheritDoc}
   */
  protected function setUp(): void {
    parent::setUp();

    // Enable URLs for media:
    \Drupal::configFactory()
      ->getEditable('media.settings')
      ->set('standalone_url', TRUE)
      ->save(TRUE);

    $this->container->get('router.builder')->rebuild();

    // Create Media Type:
    $this->createMediaType('test', [
      'id' => 'test',
      'label' => 'Test Media Type',
    ]);
  }

}
