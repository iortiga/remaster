<?php

namespace Drupal\Tests\entity_access_by_reference_field\Functional\Media;

use Drupal\media\Entity\Media;
use Drupal\user\Entity\Role;

/**
 * This class provides access tests for entity_access_by_reference_field media.
 *
 * @group entity_access_by_reference_field
 */
class MediaReferencingNodeAccessTest extends MediaEntityAccessTestBase {

  /**
   * Setup media referencing content tests.
   */
  public function setupMediaReferencingContent() {
    $this->createEntityReferenceField('media', 'test', 'field_article_test', 'field_article_test', 'node', 'default', ['target_bundles' => ['article']]);
    $this->fieldStorage = \Drupal::entityTypeManager()->getStorage('field_storage_config')->load('media.field_article_test');

    // Create a node:
    $node = $this->drupalCreateNode([
      'id' => 1,
      'title' => 'My Test Node',
      'type' => 'article',
      'body' => 'Body field value.',
    ]);
    $node->save();

    // Create Media instance:
    $media = Media::create([
      'mid' => 1,
      'bundle' => 'test',
      'uid' => $this->adminUser->id(),
      'name' => 'My Test Media',
      'field_article_test' => [
        0 => [
          'target_id' => $node->id(),
        ],
      ],
    ]);
    $media->save();
  }

  /**
   * {@inheritDoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->setupMediaReferencingContent();
  }

  /**
   * Test, if a media can be accessed.
   *
   * Tests, if a media referencing a node can be viewed, when all
   * permission matrix values are set to true.
   */
  public function testAccessViewOnMediaReferencingNodeViewAllowed() {
    $this->drupalLogin($this->authenticatedUser);

    // Allow all $permissionMatrixValues:
    $permissionMatrixValues = $this->initiatePermissionMatrix();

    // Give the permission to update the entity if the user is allowed to
    // update the referenced entity:
    $permissionMatrixValues['columns']['view']['view'] = TRUE;
    $this->setupThirdPartySettings($this->fieldStorage, 'or', TRUE, $permissionMatrixValues);

    // The view page should not be accessible:
    $this->drupalGet('/media/1');
    $this->assertSession()->statusCodeEquals(403);
    // Check if the edit page is not accessible:
    $this->drupalGet('/media/1/edit');
    $this->assertSession()->statusCodeEquals(403);
    // The delete page should not be accessible:
    $this->drupalGet('/media/1/delete');
    $this->assertSession()->statusCodeEquals(403);

    // Grant permission to view the referenced article:
    $authenticatedRole = Role::load('authenticated');
    $authenticatedRole->grantPermission('access content')->save();

    // The view page should be accessible:
    $this->drupalGet('/media/1');
    $this->assertSession()->statusCodeEquals(200);
    // The edit page should still not be accessible:
    $this->drupalGet('/media/1/edit');
    $this->assertSession()->statusCodeEquals(403);
    // The delete page should still not be accessible:
    $this->drupalGet('/media/1/delete');
    $this->assertSession()->statusCodeEquals(403);
  }

  /**
   * Test, if a media can be accessed.
   *
   * Tests, if a media referencing a node can be edited, when the user
   * is only allowed to edit the referenced entity.
   */
  public function testAccessUpdateOnMediaReferencingNodeUpdateAllowed() {
    $this->drupalLogin($this->authenticatedUser);

    // Allow all $permissionMatrixValues:
    $permissionMatrixValues = $this->initiatePermissionMatrix();

    // Give the permission to update the entity if the user is allowed to
    // update the referenced entity:
    $permissionMatrixValues['columns']['update']['update'] = TRUE;
    $this->setupThirdPartySettings($this->fieldStorage, 'or', TRUE, $permissionMatrixValues);

    // Check if the edit page is not accessible:
    $this->drupalGet('/media/1/edit');
    $this->assertSession()->statusCodeEquals(403);
    // The delete page should not be accessible:
    $this->drupalGet('/media/1/delete');
    $this->assertSession()->statusCodeEquals(403);
    // The view page should not be accessible:
    $this->drupalGet('/media/1');
    $this->assertSession()->statusCodeEquals(403);

    // Grant permission to edit the referenced article:
    $authenticatedRole = Role::load('authenticated');
    $authenticatedRole->grantPermission('edit any article content')->save();
    // We always need the access content permission for any bundle actions:
    $authenticatedRole->grantPermission('access content')->save();

    // The edit page of the media should now be also accessible:
    $this->drupalGet('/media/1/edit');
    $this->assertSession()->statusCodeEquals(200);
    // The delete page should still not be accessible:
    $this->drupalGet('/media/1/delete');
    $this->assertSession()->statusCodeEquals(403);
    // The view page should not be accessible:
    $this->drupalGet('/media/1');
    $this->assertSession()->statusCodeEquals(403);
  }

  /**
   * Test, if a media can be accessed.
   *
   * Tests, if a user can access a  media referencing a node's deletion
   * page, when the user is only allowed to delete the referenced entity.
   */
  public function testAccessDeleteOnMediaReferencingNodeDeleteAllowed() {
    $this->drupalLogin($this->authenticatedUser);

    // Allow all $permissionMatrixValues:
    $permissionMatrixValues = $this->initiatePermissionMatrix();

    // Give the permission to delete the entity if the user is allowed to
    // delete the referenced entity:
    $permissionMatrixValues['columns']['delete']['delete'] = TRUE;
    $this->setupThirdPartySettings($this->fieldStorage, 'or', TRUE, $permissionMatrixValues);

    // Check if the delete page is not accessible:
    $this->drupalGet('/media/1/delete');
    $this->assertSession()->statusCodeEquals(403);
    // The edit page should not be accessible:
    $this->drupalGet('/media/1/edit');
    $this->assertSession()->statusCodeEquals(403);
    // The view page should not be accessible:
    $this->drupalGet('/media/1');
    $this->assertSession()->statusCodeEquals(403);

    // Grant permission to delete the referenced article:
    $authenticatedRole = Role::load('authenticated');
    $authenticatedRole->grantPermission('delete any article content')->save();
    // We always need the access content permission for any bundle actions:
    $authenticatedRole->grantPermission('access content')->save();

    // Check if the delete page is now accessible:
    $this->drupalGet('/media/1/delete');
    $this->assertSession()->statusCodeEquals(200);
    // The edit page should still not be accessible:
    $this->drupalGet('/media/1/edit');
    $this->assertSession()->statusCodeEquals(403);
    // The view page should not be accessible:
    $this->drupalGet('/media/1');
    $this->assertSession()->statusCodeEquals(403);
  }

  /**
   * Test, if a media can be accessed.
   *
   * Tests, if a media referencing a node can be edited, when the user
   * is only allowed to delete the referenced entity.
   */
  public function testAccessEditOnMediaReferencingNodeDeleteAllowed() {
    $this->drupalLogin($this->authenticatedUser);

    // Allow all $permissionMatrixValues:
    $permissionMatrixValues = $this->initiatePermissionMatrix();

    // Give the permission to delete the entity if the user is allowed to
    // delete the referenced entity:
    $permissionMatrixValues['columns']['update']['delete'] = TRUE;
    $this->setupThirdPartySettings($this->fieldStorage, 'or', TRUE, $permissionMatrixValues);

    // Check if the delete page is not accessible:
    $this->drupalGet('/media/1/delete');
    $this->assertSession()->statusCodeEquals(403);
    // The edit page should not be accessible:
    $this->drupalGet('/media/1/edit');
    $this->assertSession()->statusCodeEquals(403);
    // The view page should not be accessible:
    $this->drupalGet('/media/1');
    $this->assertSession()->statusCodeEquals(403);

    // Grant permission to delete the referenced article:
    $authenticatedRole = Role::load('authenticated');
    $authenticatedRole->grantPermission('delete any article content')->save();
    // We always need the access content permission for any bundle actions:
    $authenticatedRole->grantPermission('access content')->save();

    // Check if the edit page is now accessible:
    $this->drupalGet('/media/1/edit');
    $this->assertSession()->statusCodeEquals(200);
    // The delete page should still not be accessible:
    $this->drupalGet('/media/1/delete');
    $this->assertSession()->statusCodeEquals(403);
    // The view page should not be accessible:
    $this->drupalGet('/media/1');
    $this->assertSession()->statusCodeEquals(403);
  }

  /**
   * Test, if a media can be accessed.
   *
   * Tests, if a media referencing a node can be edited, viewed and
   * deleted, when the user is only allowed to delete the referenced entity.
   */
  public function testAccessMultipleOnMediaReferencingNodeDeleteAllowed() {
    $this->drupalLogin($this->authenticatedUser);

    // Allow all $permissionMatrixValues:
    $permissionMatrixValues = $this->initiatePermissionMatrix();

    // Give the permission to delete the entity if the user is allowed to
    // delete the referenced entity:
    $permissionMatrixValues['columns']['view']['delete'] = TRUE;
    $permissionMatrixValues['columns']['update']['delete'] = TRUE;
    $permissionMatrixValues['columns']['delete']['delete'] = TRUE;
    $this->setupThirdPartySettings($this->fieldStorage, 'or', TRUE, $permissionMatrixValues);

    // Check if the delete page is not accessible:
    $this->drupalGet('/media/1/delete');
    $this->assertSession()->statusCodeEquals(403);
    // The edit page should not be accessible:
    $this->drupalGet('/media/1/edit');
    $this->assertSession()->statusCodeEquals(403);
    // The view page should not be accessible:
    $this->drupalGet('/media/1');
    $this->assertSession()->statusCodeEquals(403);

    // Grant permission to delete the referenced article:
    $authenticatedRole = Role::load('authenticated');
    $authenticatedRole->grantPermission('delete any article content')->save();
    // We always need the access content permission for any bundle actions:
    $authenticatedRole->grantPermission('access content')->save();

    // Check if the edit page is now accessible:
    $this->drupalGet('/media/1/edit');
    $this->assertSession()->statusCodeEquals(200);
    // The delete page should also be accessible now:
    $this->drupalGet('/media/1/delete');
    $this->assertSession()->statusCodeEquals(200);
    // And the view page:
    $this->drupalGet('/media/1');
    $this->assertSession()->statusCodeEquals(200);
  }

  /**
   * Test, if a media can be accessed.
   *
   * Tests, if a media referencing a node can be edited, viewed and
   * deleted, when the user is only allowed to delete the referenced entity.
   */
  public function testAccessMultipleOnMediaReferencingNodeMultipleAllowed() {
    $this->drupalLogin($this->authenticatedUser);

    // Allow all $permissionMatrixValues:
    $permissionMatrixValues = $this->initiatePermissionMatrix();

    // Allow all permissions:
    foreach ($permissionMatrixValues['columns'] as &$columnValues) {
      foreach ($columnValues as &$rowValue) {
        $rowValue = TRUE;
      }
    }
    $this->setupThirdPartySettings($this->fieldStorage, 'or', TRUE, $permissionMatrixValues);

    // Check if the delete page is not accessible:
    $this->drupalGet('/media/1/delete');
    $this->assertSession()->statusCodeEquals(403);
    // The edit page should not be accessible:
    $this->drupalGet('/media/1/edit');
    $this->assertSession()->statusCodeEquals(403);
    // The view page should not be accessible:
    $this->drupalGet('/media/1');
    $this->assertSession()->statusCodeEquals(403);

    // Grant permission to delete the referenced article:
    $authenticatedRole = Role::load('authenticated');
    // We always need the access content permission for any bundle actions:
    $authenticatedRole->grantPermission('access content')->save();
    $authenticatedRole->grantPermission('delete any article content')->save();

    // Check if the edit page is now accessible:
    $this->drupalGet('/media/1/edit');
    $this->assertSession()->statusCodeEquals(200);
    // The delete page should also be accessible now:
    $this->drupalGet('/media/1/delete');
    $this->assertSession()->statusCodeEquals(200);
    // And the view page:
    $this->drupalGet('/media/1');
    $this->assertSession()->statusCodeEquals(200);

    // Revoke old permission and add edit permission:
    $authenticatedRole->revokePermission('delete any article content')->save();
    $authenticatedRole->grantPermission('edit any article content')->save();

    // All pages should still be accessible:
    $this->drupalGet('/media/1/edit');
    $this->assertSession()->statusCodeEquals(200);

    $this->drupalGet('/media/1/delete');
    $this->assertSession()->statusCodeEquals(200);

    $this->drupalGet('/media/1');
    $this->assertSession()->statusCodeEquals(200);

    // Revoke old permission and add view permission:
    $authenticatedRole->revokePermission('edit any article content')->save();
    $authenticatedRole->grantPermission('access content')->save();

    // All pages should still be accessible:
    $this->drupalGet('/media/1/edit');
    $this->assertSession()->statusCodeEquals(200);

    $this->drupalGet('/media/1/delete');
    $this->assertSession()->statusCodeEquals(200);

    $this->drupalGet('/media/1');
    $this->assertSession()->statusCodeEquals(200);
  }

}
