<?php

namespace Drupal\Tests\entity_access_by_reference_field\Functional;

use Drupal\Tests\taxonomy\Traits\TaxonomyTestTrait;
use Drupal\user\Entity\Role;

/**
 * This class provides access tests for nodes referencing taxonomy terrms.
 *
 * Note, that we can not test the "view" properly for nodes, since "access
 * content" overrides our permission and there is no "view content type"
 * permission.
 *
 * @group entity_access_by_reference_field
 */
class NodeReferencingTaxonomyAccessTest extends EntityAccessTestBase {
  use TaxonomyTestTrait;

  /**
   * A test taxonomy.
   *
   * We need to set this vocabulary globally, as the "createVocabulary" method
   * does not have parameters, and we need the "vid" inside our tests.
   *
   * @var \Drupal\taxonomy\VocabularyInterface
   */
  protected $testTaxonomy;

  /**
   * A test term.
   *
   * @var \Drupal\taxonomy\VocabularyInterface
   */
  protected $testTerm;

  /**
   * Further modules to enable.
   *
   * @var array
   */
  protected static $modules = [
    'taxonomy',
  ];

  /**
   * Setup node referencing node tests.
   */
  public function setupNodeReferencingTaxonomyTests() {
    $this->testTaxonomy = $this->createVocabulary();
    $this->testTaxonomy->save();

    // This reference field setup is super complicated compared to the others,
    // but for some reason simply setting it up, doesn't work:
    $this->createEntityReferenceField('node', 'article', 'field_term_test', 'field_term_test', 'taxonomy_term', 'default', [
      'target_bundles' => [
        $this->testTaxonomy->id() => $this->testTaxonomy->id(),
      ],
      'auto_create' => TRUE,
      'auto_create_bundle' => $this->testTaxonomy->id(),
    ]);
    $this->fieldStorage = \Drupal::entityTypeManager()->getStorage('field_storage_config')->load('node.field_term_test');

    $this->testTerm = $this->createTerm($this->testTaxonomy);
    $this->testTerm->save();

    // Create an article referencing the taxonomy:
    $node = $this->drupalCreateNode([
      'id' => 1,
      'title' => 'My Test Node',
      'type' => 'article',
      'body' => 'Body field value.',
      'field_term_test' => [
        0 => [
          'target_id' => $this->testTerm->id(),
        ],
      ],
    ]);
    $node->save();
  }

  /**
   * {@inheritDoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->createContentType(['type' => 'notice']);
    $this->setupNodeReferencingTaxonomyTests();
  }

  /**
   * Test, if a node can be accessed.
   *
   * Tests, if a node referencing a node can be edited, when the user
   * is only allowed to edit the referenced entity.
   */
  public function testAccessUpdateOnNodeReferencingTaxonomyUpdateAllowed() {
    $this->drupalLogin($this->authenticatedUser);

    // Allow all $permissionMatrixValues:
    $permissionMatrixValues = $this->initiatePermissionMatrix();

    // Give the permission to update the entity if the user is allowed to
    // update the referenced entity:
    $permissionMatrixValues['columns']['update']['update'] = TRUE;
    $this->setupThirdPartySettings($this->fieldStorage, 'or', TRUE, $permissionMatrixValues);

    // $this->drupalGet('/admin/structure/types/manage/article/fields/node.article.field_term_test/storage');
    // $this->drupalGet('/admin/structure/types/manage/article/fields');
    // Check if the edit page is not accessible:
    $this->drupalGet('/node/1/edit');
    $this->assertSession()->statusCodeEquals(403);
    // The delete page should not be accessible:
    $this->drupalGet('/node/1/delete');
    $this->assertSession()->statusCodeEquals(403);

    // Grant permission to edit the referenced article:
    $authenticatedRole = Role::load('authenticated');
    $authenticatedRole
      ->grantPermission('edit terms in ' . $this->testTaxonomy->get('vid'))
    // We always need the access content permission for any bundle actions:
      ->grantPermission('access content')->save();

    // The edit page of the node should now be also accessible:
    $this->drupalGet('/node/1/edit');
    $this->assertSession()->statusCodeEquals(200);
    // The delete page should still not be accessible:
    $this->drupalGet('/node/1/delete');
    $this->assertSession()->statusCodeEquals(403);
  }

  /**
   * Test, if a node can be accessed.
   *
   * Tests, if a user can access a  node referencing a node's deletion
   * page, when the user is only allowed to delete the referenced entity.
   */
  public function testAccessDeleteOnNodeReferencingTaxonomyDeleteAllowed() {
    $this->drupalLogin($this->authenticatedUser);

    // Allow all $permissionMatrixValues:
    $permissionMatrixValues = $this->initiatePermissionMatrix();

    // Give the permission to delete the entity if the user is allowed to
    // delete the referenced entity:
    $permissionMatrixValues['columns']['delete']['delete'] = TRUE;
    $this->setupThirdPartySettings($this->fieldStorage, 'or', TRUE, $permissionMatrixValues);

    // Check if the delete page is not accessible:
    $this->drupalGet('/node/1/delete');
    $this->assertSession()->statusCodeEquals(403);
    // The edit page should not be accessible:
    $this->drupalGet('/node/1/edit');
    $this->assertSession()->statusCodeEquals(403);

    // Grant permission to delete the referenced article:
    $authenticatedRole = Role::load('authenticated');
    $authenticatedRole->grantPermission('delete terms in ' . $this->testTaxonomy->get('vid'))->save();
    // We always need the access content permission for any bundle actions:
    $authenticatedRole->grantPermission('access content')->save();

    // Check if the delete page is now accessible:
    $this->drupalGet('/node/1/delete');
    $this->assertSession()->statusCodeEquals(200);
    // The edit page should still not be accessible:
    $this->drupalGet('/node/1/edit');
    $this->assertSession()->statusCodeEquals(403);
  }

  /**
   * Test, if a node can be accessed.
   *
   * Tests, if a node referencing a node can be edited, when the user
   * is only allowed to delete the referenced entity.
   */
  public function testAccessEditOnNodeReferencingTaxonomyDeleteAllowed() {
    $this->drupalLogin($this->authenticatedUser);

    // Allow all $permissionMatrixValues:
    $permissionMatrixValues = $this->initiatePermissionMatrix();

    // Give the permission to delete the entity if the user is allowed to
    // delete the referenced entity:
    $permissionMatrixValues['columns']['update']['delete'] = TRUE;
    $this->setupThirdPartySettings($this->fieldStorage, 'or', TRUE, $permissionMatrixValues);

    // Check if the delete page is not accessible:
    $this->drupalGet('/node/1/delete');
    $this->assertSession()->statusCodeEquals(403);
    // The edit page should not be accessible:
    $this->drupalGet('/node/1/edit');
    $this->assertSession()->statusCodeEquals(403);

    // Grant permission to delete the referenced article:
    $authenticatedRole = Role::load('authenticated');
    $authenticatedRole->grantPermission('delete terms in ' . $this->testTaxonomy->get('vid'))->save();
    // We always need the access content permission for any bundle actions:
    $authenticatedRole->grantPermission('access content')->save();

    // Check if the edit page is now accessible:
    $this->drupalGet('/node/1/edit');
    $this->assertSession()->statusCodeEquals(200);
    // The delete page should still not be accessible:
    $this->drupalGet('/node/1/delete');
    $this->assertSession()->statusCodeEquals(403);
  }

  /**
   * Test, if a node can be accessed.
   *
   * Tests, if a node referencing a node can be edited, viewed and
   * deleted, when the user is only allowed to delete the referenced entity.
   */
  public function testAccessMultipleOnNodeReferencingTaxonomyDeleteAllowed() {
    $this->drupalLogin($this->authenticatedUser);

    // Allow all $permissionMatrixValues:
    $permissionMatrixValues = $this->initiatePermissionMatrix();

    // Give the permission to delete the entity if the user is allowed to
    // delete the referenced entity:
    $permissionMatrixValues['columns']['view']['delete'] = TRUE;
    $permissionMatrixValues['columns']['update']['delete'] = TRUE;
    $permissionMatrixValues['columns']['delete']['delete'] = TRUE;
    $this->setupThirdPartySettings($this->fieldStorage, 'or', TRUE, $permissionMatrixValues);

    // Check if the delete page is not accessible:
    $this->drupalGet('/node/1/delete');
    $this->assertSession()->statusCodeEquals(403);
    // The edit page should not be accessible:
    $this->drupalGet('/node/1/edit');
    $this->assertSession()->statusCodeEquals(403);

    // Grant permission to delete the referenced article:
    $authenticatedRole = Role::load('authenticated');
    $authenticatedRole->grantPermission('delete terms in ' . $this->testTaxonomy->get('vid'))->save();
    // We always need the access content permission for any bundle actions:
    $authenticatedRole->grantPermission('access content')->save();

    // Check if the edit page is now accessible:
    $this->drupalGet('/node/1/edit');
    $this->assertSession()->statusCodeEquals(200);
    // The delete page should also be accessible now:
    $this->drupalGet('/node/1/delete');
    $this->assertSession()->statusCodeEquals(200);
  }

  /**
   * Test, if a node can be accessed.
   *
   * Tests, if a node referencing a node can be edited, viewed and
   * deleted, when the user is only allowed to delete the referenced entity.
   */
  public function testAccessMultipleOnNodeReferencingTaxonomyMultipleAllowed() {
    $this->drupalLogin($this->authenticatedUser);

    // Allow all $permissionMatrixValues:
    $permissionMatrixValues = $this->initiatePermissionMatrix();

    // Allow all permissions:
    foreach ($permissionMatrixValues['columns'] as &$columnValues) {
      foreach ($columnValues as &$rowValue) {
        $rowValue = TRUE;
      }
    }
    $this->setupThirdPartySettings($this->fieldStorage, 'or', TRUE, $permissionMatrixValues);

    // Check if the delete page is not accessible:
    $this->drupalGet('/node/1/delete');
    $this->assertSession()->statusCodeEquals(403);
    // The edit page should not be accessible:
    $this->drupalGet('/node/1/edit');
    $this->assertSession()->statusCodeEquals(403);

    // Grant permission to delete the referenced article:
    $authenticatedRole = Role::load('authenticated');
    // We always need the access content permission for any bundle actions:
    $authenticatedRole->grantPermission('access content')->save();
    $authenticatedRole->grantPermission('delete terms in ' . $this->testTaxonomy->get('vid'))->save();

    // Check if the edit page is now accessible:
    $this->drupalGet('/node/1/edit');
    $this->assertSession()->statusCodeEquals(200);
    // The delete page should also be accessible now:
    $this->drupalGet('/node/1/delete');
    $this->assertSession()->statusCodeEquals(200);

    // Revoke old permission and add edit permission:
    $authenticatedRole->revokePermission('delete terms in ' . $this->testTaxonomy->get('vid'))->save();
    $authenticatedRole->grantPermission('edit terms in ' . $this->testTaxonomy->get('vid'))->save();

    // All pages should still be accessible:
    $this->drupalGet('/node/1/edit');
    $this->assertSession()->statusCodeEquals(200);

    $this->drupalGet('/node/1/delete');
    $this->assertSession()->statusCodeEquals(200);

    // Revoke old permission and add view permission:
    $authenticatedRole->revokePermission('edit terms in ' . $this->testTaxonomy->get('vid'))->save();
    $authenticatedRole->grantPermission('access content')->save();

    // All pages should still be accessible:
    $this->drupalGet('/node/1/edit');
    $this->assertSession()->statusCodeEquals(200);

    $this->drupalGet('/node/1/delete');
    $this->assertSession()->statusCodeEquals(200);
  }

}
