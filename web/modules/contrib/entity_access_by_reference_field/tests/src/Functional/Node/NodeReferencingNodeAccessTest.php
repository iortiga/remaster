<?php

namespace Drupal\Tests\entity_access_by_reference_field\Functional;

use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\user\Entity\Role;

/**
 * This class provides access tests for nodes referencing nodes.
 *
 * Note, that we can not test the "view" properly for nodes, since "access
 * content" overrides our permission and there is no "view content type"
 * permission.
 *
 * @group entity_access_by_reference_field
 */
class NodeReferencingNodeAccessTest extends EntityAccessTestBase {

  /**
   * Setup node referencing node tests.
   */
  public function setupNodeReferencingNodeTests() {
    $this->createEntityReferenceField('node', 'article', 'field_notice_test', 'field_notice_test', 'node', 'default', ['target_bundles' => ['notice']]);
    $this->fieldStorage = \Drupal::entityTypeManager()->getStorage('field_storage_config')->load('node.field_notice_test');

    // Create a notice:
    $referencedNode = $this->drupalCreateNode([
      'id' => 1,
      'title' => 'Notice',
      'type' => 'notice',
      'body' => 'Body field value.',
    ]);
    $referencedNode->save();

    // Create an article referencing the notice:
    $node = $this->drupalCreateNode([
      'id' => 2,
      'title' => 'My Test Node',
      'type' => 'article',
      'body' => 'Body field value.',
      'field_notice_test' => [
        0 => [
          'target_id' => $referencedNode->id(),
        ],
      ],
    ]);
    $node->save();
  }

  /**
   * Setup node referencing multiple nodes tests.
   */
  public function setupNodeReferencingMultipleNodesTests() {
    $this->createEntityReferenceField('node', 'article', 'field_notice_test', 'field_notice_test', 'node', 'default', [
      'target_bundles' => [
        'notice',
        'letter',
      ],
    ], FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED);
    $this->fieldStorage = \Drupal::entityTypeManager()->getStorage('field_storage_config')->load('node.field_notice_test');

    // Create two notices:
    $referencedNode = $this->drupalCreateNode([
      'id' => 1,
      'title' => 'Notice',
      'type' => 'notice',
      'body' => 'Body field value.',
    ]);
    $referencedNode->save();

    $referencedNode2 = $this->drupalCreateNode([
      'id' => 2,
      'title' => 'Letter',
      'type' => 'letter',
      'body' => 'Body field value.',
    ]);
    $referencedNode2->save();

    // Create an article referencing the notice:
    $node = $this->drupalCreateNode([
      'id' => 3,
      'title' => 'My Test Node',
      'type' => 'article',
      'body' => 'Body field value.',
      'field_notice_test' => [
        0 => [
          'target_id' => $referencedNode->id(),
        ],
        1 => [
          'target_id' => $referencedNode2->id(),
        ],
      ],
    ]);
    $node->save();
  }

  /**
   * {@inheritDoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->createContentType(['type' => 'notice']);
    $this->createContentType(['type' => 'letter']);
  }

  /**
   * Test, if a node can be accessed.
   *
   * Tests, if a node referencing a node can be viewed, when all
   * permission matrix values are set to true.
   */
  public function testAccessViewOnNodeReferencingNodeViewAllowed() {
    $this->setupNodeReferencingNodeTests();
    $this->drupalLogin($this->authenticatedUser);

    // Allow all $permissionMatrixValues:
    $permissionMatrixValues = $this->initiatePermissionMatrix();

    // Give the permission to update the entity if the user is allowed to
    // update the referenced entity:
    $permissionMatrixValues['columns']['view']['view'] = TRUE;
    $this->setupThirdPartySettings($this->fieldStorage, 'or', TRUE, $permissionMatrixValues);

    // The view page should not be accessible:
    $this->drupalGet('/node/2');
    $this->assertSession()->statusCodeEquals(403);
    // Check if the edit page is not accessible:
    $this->drupalGet('/node/2/edit');
    $this->assertSession()->statusCodeEquals(403);
    // The delete page should not be accessible:
    $this->drupalGet('/node/2/delete');
    $this->assertSession()->statusCodeEquals(403);

    // Grant permission to view the referenced article:
    $authenticatedRole = Role::load('authenticated');
    $authenticatedRole->grantPermission('access content')->save();

    // The view page should be accessible:
    $this->drupalGet('/node/2');
    $this->assertSession()->statusCodeEquals(200);
    // The edit page should still not be accessible:
    $this->drupalGet('/node/2/edit');
    $this->assertSession()->statusCodeEquals(403);
    // The delete page should still not be accessible:
    $this->drupalGet('/node/2/delete');
    $this->assertSession()->statusCodeEquals(403);
  }

  /**
   * Test, if a node can be accessed.
   *
   * Tests, if a node referencing a node can be edited, when the user
   * is only allowed to edit the referenced entity.
   */
  public function testAccessUpdateOnNodeReferencingNodeUpdateAllowed() {
    $this->setupNodeReferencingNodeTests();
    $this->drupalLogin($this->authenticatedUser);

    // Allow all $permissionMatrixValues:
    $permissionMatrixValues = $this->initiatePermissionMatrix();

    // Give the permission to update the entity if the user is allowed to
    // update the referenced entity:
    $permissionMatrixValues['columns']['update']['update'] = TRUE;
    $this->setupThirdPartySettings($this->fieldStorage, 'or', TRUE, $permissionMatrixValues);

    // Check if the edit page is not accessible:
    $this->drupalGet('/node/2/edit');
    $this->assertSession()->statusCodeEquals(403);
    // The delete page should not be accessible:
    $this->drupalGet('/node/2/delete');
    $this->assertSession()->statusCodeEquals(403);

    // Grant permission to edit the referenced article:
    $authenticatedRole = Role::load('authenticated');
    $authenticatedRole->grantPermission('edit any notice content')->save();
    // We always need the access content permission for any bundle actions:
    $authenticatedRole->grantPermission('access content')->save();

    // The edit page of the node should now be also accessible:
    $this->drupalGet('/node/2/edit');
    $this->assertSession()->statusCodeEquals(200);
    // The delete page should still not be accessible:
    $this->drupalGet('/node/2/delete');
    $this->assertSession()->statusCodeEquals(403);
  }

  /**
   * Test, if a node can be accessed.
   *
   * Tests, if a node referencing multiple nodes can be edited, when the
   * user is only allowed to edit one of the referenced entities, and the
   * multiple entities behavior is set to 'or'.
   */
  public function testAccessUpdateOnNodeReferencingMultipleNodesBehaviorOrUpdateAllowed() {
    $this->setupNodeReferencingMultipleNodesTests();
    $this->drupalLogin($this->authenticatedUser);

    // Allow all $permissionMatrixValues:
    $permissionMatrixValues = $this->initiatePermissionMatrix();

    // Give the permission to update the entity if the user is allowed to
    // update the referenced entity:
    $permissionMatrixValues['columns']['update']['update'] = TRUE;
    $this->setupThirdPartySettings($this->fieldStorage, 'or', TRUE, $permissionMatrixValues);

    // Check if the edit page is not accessible:
    $this->drupalGet('/node/3/edit');
    $this->assertSession()->statusCodeEquals(403);
    // The delete page should not be accessible:
    $this->drupalGet('/node/3/delete');
    $this->assertSession()->statusCodeEquals(403);

    // Grant permission to edit the referenced article:
    $authenticatedRole = Role::load('authenticated');
    $authenticatedRole->grantPermission('edit any notice content')->save();
    // We always need the access content permission for any bundle actions:
    $authenticatedRole->grantPermission('access content')->save();

    // The edit page of the node should now be also accessible:
    $this->drupalGet('/node/3/edit');
    $this->assertSession()->statusCodeEquals(200);
    // The delete page should still not be accessible:
    $this->drupalGet('/node/3/delete');
    $this->assertSession()->statusCodeEquals(403);
  }

  /**
   * Test, if a node can be accessed.
   *
   * Tests, if a node referencing multiple nodes can be edited, when the
   * user is allowed to edit both of the referenced entities, and the
   * multiple entities behavior is set to 'and'.
   */
  public function testAccessUpdateOnNodeReferencingMultipleNodesBehaviorAndUpdateAllowed() {
    $this->setupNodeReferencingMultipleNodesTests();
    $this->drupalLogin($this->authenticatedUser);

    // Allow all $permissionMatrixValues:
    $permissionMatrixValues = $this->initiatePermissionMatrix();

    // Give the permission to update the entity if the user is allowed to
    // update the referenced entity:
    $permissionMatrixValues['columns']['update']['update'] = TRUE;
    $this->setupThirdPartySettings($this->fieldStorage, 'and', TRUE, $permissionMatrixValues);

    // Check if the edit page is not accessible:
    $this->drupalGet('/node/3/edit');
    $this->assertSession()->statusCodeEquals(403);
    // The delete page should not be accessible:
    $this->drupalGet('/node/3/delete');
    $this->assertSession()->statusCodeEquals(403);

    // Grant permission to edit the referenced article:
    $authenticatedRole = Role::load('authenticated');
    $authenticatedRole->grantPermission('edit any notice content')->save();
    $authenticatedRole->grantPermission('edit any letter content')->save();
    // We always need the access content permission for any bundle actions:
    $authenticatedRole->grantPermission('access content')->save();

    // The edit page of the node should now be also accessible:
    $this->drupalGet('/node/3/edit');
    $this->assertSession()->statusCodeEquals(200);
    // The delete page should still not be accessible:
    $this->drupalGet('/node/3/delete');
    $this->assertSession()->statusCodeEquals(403);
  }

  /**
   * Test, if a node can not be accessed.
   *
   * Tests, if a node referencing multiple nodes can not be edited, when the
   * user is only allowed to edit one of the referenced entities, and the
   * multiple entities behavior is set to 'and'.
   */
  public function testNoAccessUpdateOnNodeReferencingMultipleNodesBehaviorAndUpdateAllowed() {
    $this->setupNodeReferencingMultipleNodesTests();
    $this->drupalLogin($this->authenticatedUser);

    // Allow all $permissionMatrixValues:
    $permissionMatrixValues = $this->initiatePermissionMatrix();

    // Give the permission to update the entity if the user is allowed to
    // update the referenced entity:
    $permissionMatrixValues['columns']['update']['update'] = TRUE;
    $this->setupThirdPartySettings($this->fieldStorage, 'and', TRUE, $permissionMatrixValues);

    // Check if the edit page is not accessible:
    $this->drupalGet('/node/3/edit');
    $this->assertSession()->statusCodeEquals(403);
    // The delete page should not be accessible:
    $this->drupalGet('/node/3/delete');
    $this->assertSession()->statusCodeEquals(403);

    // Grant permission to edit the referenced article:
    $authenticatedRole = Role::load('authenticated');
    $authenticatedRole->grantPermission('edit any notice content')->save();
    // We always need the access content permission for any bundle actions:
    $authenticatedRole->grantPermission('access content')->save();

    // The edit page of the node should now be also accessible:
    $this->drupalGet('/node/3/edit');
    $this->assertSession()->statusCodeEquals(403);
    // The delete page should still not be accessible:
    $this->drupalGet('/node/3/delete');
    $this->assertSession()->statusCodeEquals(403);
  }

  /**
   * Test, if a node can be accessed.
   *
   * Tests, if a user can access a  node referencing a node's deletion
   * page, when the user is only allowed to delete the referenced entity.
   */
  public function testAccessDeleteOnNodeReferencingNodeDeleteAllowed() {
    $this->setupNodeReferencingNodeTests();
    $this->drupalLogin($this->authenticatedUser);

    // Allow all $permissionMatrixValues:
    $permissionMatrixValues = $this->initiatePermissionMatrix();

    // Give the permission to delete the entity if the user is allowed to
    // delete the referenced entity:
    $permissionMatrixValues['columns']['delete']['delete'] = TRUE;
    $this->setupThirdPartySettings($this->fieldStorage, 'or', TRUE, $permissionMatrixValues);

    // Check if the delete page is not accessible:
    $this->drupalGet('/node/2/delete');
    $this->assertSession()->statusCodeEquals(403);
    // The edit page should not be accessible:
    $this->drupalGet('/node/2/edit');
    $this->assertSession()->statusCodeEquals(403);

    // Grant permission to delete the referenced article:
    $authenticatedRole = Role::load('authenticated');
    $authenticatedRole->grantPermission('delete any notice content')->save();
    // We always need the access content permission for any bundle actions:
    $authenticatedRole->grantPermission('access content')->save();

    // Check if the delete page is now accessible:
    $this->drupalGet('/node/2/delete');
    $this->assertSession()->statusCodeEquals(200);
    // The edit page should still not be accessible:
    $this->drupalGet('/node/2/edit');
    $this->assertSession()->statusCodeEquals(403);
  }

  /**
   * Test, if a node can be accessed.
   *
   * Tests, if a node referencing a node can be edited, when the user
   * is only allowed to delete the referenced entity.
   */
  public function testAccessEditOnNodeReferencingNodeDeleteAllowed() {
    $this->setupNodeReferencingNodeTests();
    $this->drupalLogin($this->authenticatedUser);

    // Allow all $permissionMatrixValues:
    $permissionMatrixValues = $this->initiatePermissionMatrix();

    // Give the permission to delete the entity if the user is allowed to
    // delete the referenced entity:
    $permissionMatrixValues['columns']['update']['delete'] = TRUE;
    $this->setupThirdPartySettings($this->fieldStorage, 'or', TRUE, $permissionMatrixValues);

    // Check if the delete page is not accessible:
    $this->drupalGet('/node/2/delete');
    $this->assertSession()->statusCodeEquals(403);
    // The edit page should not be accessible:
    $this->drupalGet('/node/2/edit');
    $this->assertSession()->statusCodeEquals(403);

    // Grant permission to delete the referenced article:
    $authenticatedRole = Role::load('authenticated');
    $authenticatedRole->grantPermission('delete any notice content')->save();
    // We always need the access content permission for any bundle actions:
    $authenticatedRole->grantPermission('access content')->save();

    // Check if the edit page is now accessible:
    $this->drupalGet('/node/2/edit');
    $this->assertSession()->statusCodeEquals(200);
    // The delete page should still not be accessible:
    $this->drupalGet('/node/2/delete');
    $this->assertSession()->statusCodeEquals(403);
  }

  /**
   * Test, if a node can be accessed.
   *
   * Tests, if a node referencing a node can be edited, viewed and
   * deleted, when the user is only allowed to delete the referenced entity.
   */
  public function testAccessMultipleOnNodeReferencingNodeDeleteAllowed() {
    $this->setupNodeReferencingNodeTests();
    $this->drupalLogin($this->authenticatedUser);

    // Allow all $permissionMatrixValues:
    $permissionMatrixValues = $this->initiatePermissionMatrix();

    // Give the permission to delete the entity if the user is allowed to
    // delete the referenced entity:
    $permissionMatrixValues['columns']['view']['delete'] = TRUE;
    $permissionMatrixValues['columns']['update']['delete'] = TRUE;
    $permissionMatrixValues['columns']['delete']['delete'] = TRUE;
    $this->setupThirdPartySettings($this->fieldStorage, 'or', TRUE, $permissionMatrixValues);

    // Check if the delete page is not accessible:
    $this->drupalGet('/node/2/delete');
    $this->assertSession()->statusCodeEquals(403);
    // The edit page should not be accessible:
    $this->drupalGet('/node/2/edit');
    $this->assertSession()->statusCodeEquals(403);

    // Grant permission to delete the referenced article:
    $authenticatedRole = Role::load('authenticated');
    $authenticatedRole->grantPermission('delete any notice content')->save();
    // We always need the access content permission for any bundle actions:
    $authenticatedRole->grantPermission('access content')->save();

    // Check if the edit page is now accessible:
    $this->drupalGet('/node/2/edit');
    $this->assertSession()->statusCodeEquals(200);
    // The delete page should also be accessible now:
    $this->drupalGet('/node/2/delete');
    $this->assertSession()->statusCodeEquals(200);
  }

  /**
   * Test, if a node can be accessed.
   *
   * Tests, if a node referencing a node can be edited, viewed and
   * deleted, when the user is only allowed to delete the referenced entity.
   */
  public function testAccessMultipleOnNodeReferencingNodeMultipleAllowed() {
    $this->setupNodeReferencingNodeTests();
    $this->drupalLogin($this->authenticatedUser);

    // Allow all $permissionMatrixValues:
    $permissionMatrixValues = $this->initiatePermissionMatrix();

    // Allow all permissions:
    foreach ($permissionMatrixValues['columns'] as &$columnValues) {
      foreach ($columnValues as &$rowValue) {
        $rowValue = TRUE;
      }
    }
    $this->setupThirdPartySettings($this->fieldStorage, 'or', TRUE, $permissionMatrixValues);

    // Check if the delete page is not accessible:
    $this->drupalGet('/node/2/delete');
    $this->assertSession()->statusCodeEquals(403);
    // The edit page should not be accessible:
    $this->drupalGet('/node/2/edit');
    $this->assertSession()->statusCodeEquals(403);

    // Grant permission to delete the referenced article:
    $authenticatedRole = Role::load('authenticated');
    // We always need the access content permission for any bundle actions:
    $authenticatedRole->grantPermission('access content')->save();
    $authenticatedRole->grantPermission('delete any notice content')->save();

    // Check if the edit page is now accessible:
    $this->drupalGet('/node/2/edit');
    $this->assertSession()->statusCodeEquals(200);
    // The delete page should also be accessible now:
    $this->drupalGet('/node/2/delete');
    $this->assertSession()->statusCodeEquals(200);

    // Revoke old permission and add edit permission:
    $authenticatedRole->revokePermission('delete any notice content')->save();
    $authenticatedRole->grantPermission('edit any notice content')->save();

    // All pages should still be accessible:
    $this->drupalGet('/node/2/edit');
    $this->assertSession()->statusCodeEquals(200);

    $this->drupalGet('/node/2/delete');
    $this->assertSession()->statusCodeEquals(200);

    // Revoke old permission and add view permission:
    $authenticatedRole->revokePermission('edit any notice content')->save();
    $authenticatedRole->grantPermission('access content')->save();

    // All pages should still be accessible:
    $this->drupalGet('/node/2/edit');
    $this->assertSession()->statusCodeEquals(200);

    $this->drupalGet('/node/2/delete');
    $this->assertSession()->statusCodeEquals(200);
  }

}
