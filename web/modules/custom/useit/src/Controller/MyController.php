<?php
namespace Drupal\useit\Controller;

use Dompdf\Dompdf;
use Dompdf\Options;
use Drupal\Core\Controller\ControllerBase;
use Drupal\node\Entity\Node;
use Symfony\Component\HttpFoundation\Response;

class MyController extends ControllerBase {

  public function generatePDF($id) {
    // Cargar el nodo utilizando el ID proporcionado

    $node = Node::load($id);

    // Verificar que el tipo de nodo sea "coc" y que sea un nodo existente
    if ($node && $node->getType() === 'cars') {
      // Obtener los datos del nodo
      $title = $node->getTitle();

      $marca = '';
      $term_reference_field = $node->get('field_cars_brand');
      if (!$term_reference_field->isEmpty()) {
        $term = $term_reference_field->referencedEntities()[0];
        $marca = $term->getName();
      }
      $modelo = $node->get('field_cars_model')->value;
      $imagen_url = $node->get('field_cars_img')->entity->getFileUri();
      $imagen_url = 'https://www.dzoom.org.es/wp-content/uploads/2010/09/paisaje-profundidad-lineas-734x489.jpg';
      $img_b64 = "data:image/jpeg;base64," . base64_encode(file_get_contents($imagen_url)) ;



      // Crear el contenido HTML para el PDF
      $html = '
        <h1>'.$title.'</h1>
        <p>Marca: ' . $marca . '</p>
        <p>Modelo: ' . $modelo . '</p>
        <img src="'.$img_b64.'" />
        ';
      dpm($html);








      // Crear una instancia de Dompdf

      $options = new Options();
      $options->set('isRemoteEnabled', TRUE);
      $options->set('isHtml5ParserEnabled', TRUE);
      $options->set('isHtml5ParserEnabled', TRUE);
      $options->set('DOMPDF_ENABLE_CSS_FLOAT', TRUE);

      $dompdf = new Dompdf($options);

      $context = stream_context_create([
        'ssl' => [
          'verify_peer' => FALSE,
          'verify_peer_name' => FALSE,
          'allow_self_signed'=> TRUE
        ]
      ]);
      $dompdf->setHttpContext($context);



      // Cargar el contenido HTML en Dompdf
      $dompdf->loadHtml($html);



      // Renderizar el PDF
      $dompdf->render();
      $dompdf->stream();
      // Obtener el contenido del PDF renderizado
      $output = $dompdf->output();

      // Enviar el PDF al navegador utilizando un objeto de respuesta
      $response = new Response($output);
      $response->headers->set('Content-Type', 'application/pdf');

      return $response;
    }

    // Si no se encuentra el nodo o no cumple las condiciones, mostrar un mensaje de error
    return ['#markup' => $this->t('Error: Node not found or invalid type.')];
  }

}
