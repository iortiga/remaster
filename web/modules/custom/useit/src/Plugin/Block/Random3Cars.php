<?php

namespace Drupal\useit\Plugin\Block;

use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\node\NodeInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a 'Random 3 Cars' block.
 *
 * @Block(
 *   id = "Random3Cars",
 *   admin_label = @Translation("Random 3 Cars Block"),
 *   category = @Translation("Custom"),
 * )
 */
class Random3Cars extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The node storage.
   *
   * @var EntityStorageInterface
   */
  protected EntityStorageInterface $nodeStorage;

  /**
   * Constructs a new Random3Cars block.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    try {
      $this->nodeStorage = $entity_type_manager->getStorage('node');
    } catch (InvalidPluginDefinitionException|PluginNotFoundException $e) {
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $query = $this->nodeStorage->getQuery()
      ->condition('type', 'cars')
      ->condition('status', NodeInterface::PUBLISHED)
      ->addTag('node_access')
      ->addMetaData('account', \Drupal::currentUser())
      ->accessCheck(TRUE);

    $nids = $query->execute();
    $random_nids = $this->getRandomNids($nids, 3);
    $nodes = $this->nodeStorage->loadMultiple($random_nids);

    // Renderizar los nodos utilizando la plantilla.
    $build = [
      '#theme' => 'Random3Cars',
      '#nodes' => $nodes,
    ];

    return $build;
  }

  /**
   * Get random nids from an array of nids.
   *
   * @param array $nids
   *   Array of node ids.
   * @param int $count
   *   Number of random ids to retrieve.
   *
   * @return array
   *   Array of random node ids.
   */
  private function getRandomNids(array $nids, int $count) {
    $total_nids = count($nids);
    $random_keys = array_rand($nids, min($count, $total_nids));
    $random_nids = [];
    if (is_array($random_keys)) {
      foreach ($random_keys as $random_key) {
        $random_nids[] = $nids[$random_key];
      }
    } elseif ($random_keys !== null) {
      $random_nids[] = $nids[$random_keys];
    }
    return $random_nids;
  }


}
