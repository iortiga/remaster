<?php

namespace Drupal\useit\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormBuilderInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a custom form block.
 *
 * @Block(
 *   id = "custom_form_block_csv",
 *   admin_label = @Translation("Upload cars file"),
 *   category = @Translation("A Custom Blocks")
 * )
 */
class CustomFormBlockCsv extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The form builder service.
   *
   * @var FormBuilderInterface
   */
  protected $formBuilder;

  /**
   * Constructs a new CustomFormBlock instance.
   *
   * @param array $configuration
   *   The plugin configuration.
   * @param string $plugin_id
   *   The plugin ID.
   * @param mixed $plugin_definition
   *   The plugin definition.
   * @param FormBuilderInterface $form_builder
   *   The form builder service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, FormBuilderInterface $form_builder) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->formBuilder = $form_builder;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('form_builder')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    // Construye el formulario personalizado.
    $form = $this->formBuilder->getForm('Drupal\useit\Form\CustomFormCsv');

    // Envuelve el formulario en un contenedor.
    $build = [
      'formulario_personalizado' => [
        '#type' => 'container',
        '#attributes' => ['class' => ['custom-form']],
        'form' => $form,
      ],
    ];

    return $build;
  }

}
