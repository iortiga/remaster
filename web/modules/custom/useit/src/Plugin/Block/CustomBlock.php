<?php

namespace Drupal\useit\Plugin\Block;

use Drupal;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Block\BlockPluginInterface;
use Drupal\taxonomy\Entity\Term;


/**
 * Provides a custom block.
 *
 * @Block(
 *   id = "useit_pie_block",
 *   admin_label = @Translation("My Custom Block"),
 *   category = @Translation("Custom Blocks")
 * )
 */
class CustomBlock extends BlockBase implements BlockPluginInterface
{

  /**
   * {@inheritdoc}
   */

  public function getFields()
  {

    $query = Drupal::entityQuery('node')
      ->accessCheck(TRUE)
      ->condition('type', 'cars')
      ->execute();

    $nodes = Drupal::entityTypeManager()
      ->getStorage('node')
      ->loadMultiple($query);

    $brands = [];
    foreach ($nodes as $node) {

      // Get the value of the node field
      $field_brand = $node->get('field_cars_brand')->getValue();
      foreach ($field_brand as $item) {

        // Retrieve the taxonomy term value
        $term = Term::load($item['target_id']);
        if ($term) {
          $brands[] = $term->getName();
        }
      }
    }

    $count = array_count_values($brands);
    $brands = array_keys($count);
    $numbers = array_values($count);

    $html = $this->getPie($brands, $numbers, $count);

    return $html;
  }

  public function build()
  {
    $attached = [
      'library' => [
        'useit/graficos',
      ],
    ];
    $html = $this->getFields();
    return [
      //'#markup' => $content,
      '#type' => 'inline_template',
      '#template' => $html,
      '#attached' => $attached,
    ];
  }

  public function getPie($brands, $numbers, $cars)
  {
    $brands = implode('","', $brands);
    $numbers = implode(',', $numbers);

    $html = '<canvas id="myChart" style="width:100%;max-width:1000px"></canvas>

            <script>
            document.addEventListener("DOMContentLoaded", function() {
              var ctx = document.getElementById("myChart").getContext("2d");
              var myChart = new Chart(ctx, {
                type: "pie",
                data: {
                  labels: ["' . $brands . '"],
                  datasets: [{
                    backgroundColor: [';
    $colors = '';
    foreach ($cars as $col) {
      $color = '"' . '#' . str_pad(dechex(mt_rand(0, 0xFFFFFF)), 6, '0', STR_PAD_LEFT) . '"';
      $colors .= $color . ',';
    }
    $colors = rtrim($colors, ',');

    $html .= $colors . '],
                    data: [' . $numbers . ']
                  }]
                },
                options: {
                  title: {
                    display: true,
                    text: "Best-selling cars of this year"
                  }
                }
              });
            });
            </script>';

    $this->log($html);

    return $html;
  }

  function log($data)
  {
    $output = json_encode($data);


  }
}





