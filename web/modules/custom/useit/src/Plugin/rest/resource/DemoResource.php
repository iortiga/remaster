<?php

namespace Drupal\useit\Plugin\rest\resource;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Entity\EntityStorageException;
use Drupal\Core\Executable\ExecutableException;
use Drupal\file\Entity\File;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;
use Drupal\taxonomy\Entity\Term;
use http\Client\Response;


/**
 * Provides a Demo Resource
 *
 * @RestResource(
 *   id = "demo_files_up",
 *   label = @Translation("Demo files up"),
 *   uri_paths = {
 *      "canonical" = "/test/get",
 *      "create" = "/test/post"
 *   }
 * )
 */
class DemoResource extends ResourceBase
{
  /**
   * Responds to entity GET requests.
   * @return ResourceResponse
   */
  public function get()
  {
    $response = ['message' => 'Hello, this is a rest service'];
    return new ResourceResponse($response);
  }

  /**
   * POST
   */
  public function post($data){
    $response = new AjaxResponse();

    if ($data['id'][0]['value'] == "1") {
      // Declare the term name and vocabulary ID
      $term_name = $data['name'][0]['value'];
      $vid = $data['vid'][0]['target_id'];

      // Check if the term already exists
      $storage = \Drupal::entityTypeManager()->getStorage('taxonomy_term');
      $terms = $storage->loadByProperties([
        'name' => $term_name,
        'vid' => $vid,
      ]);

      if ($terms) {
        $term = reset($terms);
      } else {
        $term = Term::create([
          'name' => $term_name,
          'vid' => $vid,
        ]);
        $term->save();
      }
      return new ResourceResponse($term->id());
  } else {
      // Obtener la URL de la imagen
      $imageUrl = $data['url'];
      //$imageUrl = 'https://img.freepik.com/free-photo/space-background-realistic-starry-night-cosmos-shining-stars-milky-way-stardust-color-galaxy_1258-154643.jpg';
      // Obtener el nombre del archivo de la URL
      $filename = basename($imageUrl);
      // Guardar la imagen desde la URL en la carpeta de archivos públicos
      $destination = 'public://2023-06/' . $filename;
      $fileContents = file_get_contents($imageUrl);
      file_put_contents($destination, $fileContents);
      $extension = pathinfo($filename, PATHINFO_EXTENSION);
      $mime = 'image/' . $extension;
      try{
        $file = File::create(array(
          'filename' => $filename,
          'uid' => '1',
          'filemime' => $mime,
          'filesize' => '4000',
          'uri' => $destination,
        ));
        $file->save();
        $id = $file->id();
        return new ResourceResponse($id);
      } catch (ExecutableException|EntityStorageException $e) {
        \Drupal::logger('custom-rest')->error($e->getMessage());
      }
    }
  }
}
