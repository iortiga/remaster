<?php

namespace Drupal\useit\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Formulario personalizado.
 */
class CustomForm extends FormBase {

  /**
   * The current user.
   *
   * @var AccountProxyInterface
   */
  protected $currentUser;

  /**
   * Constructs a new CustomForm instance.
   *
   * @param AccountProxyInterface $current_user
   *   The current user.
   */
  public function __construct(AccountProxyInterface $current_user) {
    $this->currentUser = $current_user;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('current_user')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'custom_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // Verifica si el usuario actual tiene el rol "soci".
    $hasSociRole = $this->checkUserRole('Soci');

    // Verifica si el usuario tiene el rol "soci" y muestra el formulario.
    if ($hasSociRole) {
      $form['nombre'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Nombre'),
        '#required' => TRUE,
      ];

      $form['telefono'] = [
        '#type' => 'tel',
        '#title' => $this->t('Teléfono'),
        '#required' => TRUE,
      ];

      // Obtén el correo electrónico del usuario actual.
      $correo = $this->currentUser->getEmail();

      // Obtén el título del nodo actual.
      $node = \Drupal::routeMatch()->getParameter('node');
      $vehiculo_interes = $node->getTitle();

      // Almacena los valores en el estado del formulario.
      $form_state->set('correo', $correo);
      $form_state->set('vehiculo_interes', $vehiculo_interes);

      $form['submit'] = [
        '#type' => 'submit',
        '#value' => $this->t('Enviar'),
      ];
    } else {
      $form['message'] = [
        '#markup' => $this->t('Este formulario solo está disponible para usuarios con el rol "soci".'),
      ];
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Obtén los valores del formulario.
    $values = $form_state->getValues();

    // Obtén los valores almacenados en el estado del formulario.
    $correo = $form_state->get('correo');
    $vehiculo_interes = $form_state->get('vehiculo_interes');

    // Crea un array con los datos a enviar al log.
    $datos = [
      'nombre' => $values['nombre'],
      'telefono' => $values['telefono'],
      'correo' => $correo,
      'vehiculo_interes' => $vehiculo_interes,
    ];

    // Registra un mensaje en el log.
    //\Drupal::logger('permisos')->notice('Datos del formulario: @datos', ['@datos' => print_r($datos, TRUE)]);
    \Drupal::logger('permisos')->notice('Datos del formulario: Nombre:'.$datos['nombre'].', Tlf:'.$datos['telefono'].', Mail:'.$datos['correo'].', Ficha:'.$datos['vehiculo_interes']);
  }

  /**
   * Verifica si el usuario actual tiene un rol específico.
   *
   * @param string $role
   *   The role to check.
   *
   * @return bool
   *   TRUE if the user has the role, FALSE otherwise.
   */
  protected function checkUserRole($role) {

    $uid = $this->currentUser->id();
    $query = \Drupal::entityQuery('user');
    $query->condition('uid', $uid);
    $query->condition('roles', $role);
    $query->accessCheck(FALSE);
    $result = $query->execute();

    return !empty($result);
  }

}
