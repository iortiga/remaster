<?php

namespace Drupal\useit\Form;

use Drupal\Core\Batch\BatchBuilder;
use Drupal\Core\Entity\EntityStorageException;
use Drupal\Core\Executable\ExecutableException;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\file\FileInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\file\Entity\File;
use Drupal\Core\File\FileSystemInterface;
use Drupal\node\Entity\Node;

/**
 * Formulario personalizado.
 */
class CustomFormCsv extends FormBase {

  /**
   * The current user.
   *
   * @var AccountProxyInterface
   */
  protected AccountProxyInterface $currentUser;

  /**
   * @param AccountProxyInterface $current_user
   *   The current user.
   */
  public function __construct(AccountProxyInterface $current_user) {
    $this->currentUser = $current_user;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('current_user')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'custom_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // Verifica si el usuario tiene el rol "soci" y muestra el formulario.

    $form['file'] = [
      '#type' => 'managed_file',
      '#title' => $this->t('Archivo - CSV'),
      '#upload_location' => 'public://tmp/csv',
      '#upload_validators' => [
        'file_validate_extensions' => ['csv'],
      ],
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Enviar'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Acciones del submit

    $form_file = $form_state->getValue('file', 0);
    if (!empty($form_file[0])) {
      $file = File::load($form_file[0]);

      // Verificar si se cargó correctamente el archivo.
      if ($file instanceof FileInterface) {
        $file->setTemporary();

        // Obtener la ruta del archivo temporal.
        $file_path = $file->getFileUri();

        // Crear un objeto BatchBuilder para definir el proceso de lote.
        $batch_builder = new BatchBuilder();

        // Leer el contenido del archivo CSV.
        if (($handle = fopen($file_path, 'r')) !== FALSE) {
          // Omitir la primera línea (encabezados).
          fgetcsv($handle);

          // Recorrer cada fila del CSV.
          while (($data = fgetcsv($handle, 1000, ',')) !== FALSE) {
            $batch_builder->addOperation([$this, 'importNode'], [$data]);
          }

          fclose($handle);
        }
        else {
          // El archivo no se cargó correctamente. Agrega una lógica de manejo de errores adecuada aquí.
          \Drupal::messenger()->addError($this->t('No se pudo cargar el archivo.'));
        }

        // Establecer la función de finalización del lote.
        $batch_builder->setFinishCallback([$this, 'batchProcessFinished']);

        // Ejecutar el lote.
        batch_set($batch_builder->toArray());
      }
      else {
        // No se seleccionó ningún archivo. Agrega una lógica de manejo de errores adecuada aquí.
        \Drupal::messenger()->addError($this->t('No se seleccionó ningún archivo.'));
      }
    }
  }

  /**
   * Función de operación para importar un nodo en el lote.
   */
  public function importNode($data, &$context) {
    $title = $data[0];
    $marca = $data[1];
    $model = $data[2];
    $color = $data[3];
    $img_url = $data[4];

    //$imageUrl = 'https://img.freepik.com/free-photo/space-background-realistic-starry-night-cosmos-shining-stars-milky-way-stardust-color-galaxy_1258-154643.jpg';
    // Obtener el nombre del archivo de la URL
    $filename = basename($img_url);

    // Guardar la imagen desde la URL en la carpeta de archivos públicos
    $destination = 'public://2023-06/' . $filename;
    $fileContents = file_get_contents($img_url);
    file_put_contents($destination, $fileContents);

    $extension = pathinfo($filename, PATHINFO_EXTENSION);
    $mime = 'image/' . $extension;

    try {
      $file = File::create([
        'filename' => $filename,
        'uid' => '2559595',
        'filemime' => $mime,
        'filesize' => '4000',
        'uri' => $destination,
      ]);

      $file->save();
      $id = $file->id();
    }
    catch (EntityStorageException $e) {
      \Drupal::logger('custom-rest')->error($e->getMessage());
    }
    $node = Node::create([
      'type' => 'cars',
      'title' => $title,
      'field_cars_brand' => [
        'target_id' => $marca,
      ],
      'field_cars_model' => $model,
      'field_cars_color' => $color,
      'field_cars_img' => [
        'target_id' => $id,
      ],
    ]);

    // Guardar el nodo.
    $node->save();

    // Incrementar el progreso del lote.
    $context['results']['processed'] = isset($context['results']['processed']) ? $context['results']['processed'] + 1 : 1;
  }

  /**
   * Función de finalización del lote.
   */
  public function batchProcessFinished($success, $results, $operations) {
    if ($success) {
      \Drupal::messenger()->addStatus($this->t('Importación completada exitosamente.'));
    }
    else {
      \Drupal::messenger()->addError($this->t('Se produjo un error durante la importación.'));
    }
  }

}
