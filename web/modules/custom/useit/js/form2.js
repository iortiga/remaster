console.log("si");
alert("Adios");
(function ($) {
  Drupal.behaviors.myModule = {
    attach: function (context, settings) {
      // Obtén los campos de taxonomía y modelo.
      var taxField = $('#edit-field-cars-marca', context);
      var modelField = $('#edit-field-cars-model', context);

      // Oculta el campo de modelo inicialmente.
      modelField.hide();

      // Función para mostrar u ocultar el campo de modelo según el estado del campo de taxonomía.
      var toggleModelField = function () {
        if (taxField.val()) {
          // El campo de taxonomía tiene un valor, muestra el campo de modelo.
          modelField.show();
        } else {
          // El campo de taxonomía está vacío, oculta el campo de modelo y lo vacía.
          modelField.hide();
          modelField.val('');
        }
      };

      // Verifica el estado inicial del campo de taxonomía.
      toggleModelField();

      // Configura un evento de cambio en el campo de taxonomía para actualizar el campo de modelo.
      taxField.change(function () {
        toggleModelField();
      });
    }
  };
})(jQuery);
