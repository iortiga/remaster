<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* __string_template__ab04c0f31c643be03ec7a0cc4e99908c */
class __TwigTemplate_2ed10437cdbe293e0a5922e50e1eb026 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $this->checkSecurity();
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<canvas id=\"myChart\" style=\"width:100%;max-width:1000px\"></canvas>

            <script>
            document.addEventListener(\"DOMContentLoaded\", function() {
              var ctx = document.getElementById(\"myChart\").getContext(\"2d\");
              var myChart = new Chart(ctx, {
                type: \"pie\",
                data: {
                  labels: [\"Toyota\",\"Seat\",\"Tesla\",\"Nissan\"],
                  datasets: [{
                    backgroundColor: [\"#91238f\",\"#470807\",\"#9c8550\",\"#91988a\"],
                    data: [1,2,1,1]
                  }]
                },
                options: {
                  title: {
                    display: true,
                    text: \"Best-selling cars of this year\"
                  }
                }
              });
            });
            </script>";
    }

    public function getTemplateName()
    {
        return "__string_template__ab04c0f31c643be03ec7a0cc4e99908c";
    }

    public function getDebugInfo()
    {
        return array (  39 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "__string_template__ab04c0f31c643be03ec7a0cc4e99908c", "");
    }
    
    public function checkSecurity()
    {
        static $tags = array();
        static $filters = array();
        static $functions = array();

        try {
            $this->sandbox->checkSecurity(
                [],
                [],
                []
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->source);

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }
}
